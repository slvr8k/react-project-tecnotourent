import Footer from "./Footer";
import {useContext, useEffect, useState} from "react";
import {ProductContext} from "./ProductsContext";

export default function Layout({children}){
  const {setSelectedProducts} = useContext(ProductContext);
  const [success,setSuccess] = useState(false);
  useEffect(() => {
    if (window.location.href.includes('success=true')) {
      setSelectedProducts([]);
      setSuccess(true);
    }
  }, []);
  return (
    <div>
      <div className="p-5">
      {success && (
          <div className="mb-5 bg-green-400 text-white text-lg p-5 rounded-xl">
            ¡Gracias por realizar su pedido!
          </div>
        )}
        {children}
      </div>
      <div>
        <Footer/>
      </div>
    </div>
  )
}